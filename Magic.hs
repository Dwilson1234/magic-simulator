{-# LANGUAGE TemplateHaskell #-}
module Magic where

import Control.Lens
import Control.Monad.Trans.Class
import Control.Monad.Trans.State
import Control.Monad.IO.Class
import TStateT hiding (step)
import qualified TStateT as T (step)
import State

main = runTStateT (rules, properties) (sequence_ . cycle $ [go]) initialState

go :: TStateT MagicGame IO ()
go =
    do  s <- TStateT (gets fst)
        liftIO $ print s
        io <- TStateT $ gets $ view ioZone . fst
        t <- case io of 
                Just a -> liftIO a
                Nothing -> return (Temporary (Application (const False), Removal (const True), Transition id))
        addTransition t
        T.step

-- ability targets are lenses?
-- things go on the stack, then when it's time to pop them off the stack, they go in the transition table as (const True, const True, f), so they are applied immediately and removed immediately.
-- Counterspells would go on the stack and during resolution would remove something from the stack, making sure it never ends up in the transition table
-- Modeling continous effects as transitions
-- * Figure out when the effect is applicable e.g. All white creatures get +1/+1. Applicable anytime the P/T is inspected. That is the application function.
-- * Related, temporary copies created whenever an object is inspected? That way transitions wouldn't have to include a "reversing" function.
-- * Related, look up automatically determining bijections.
-- * Zone for continuous effects? I think most can have removal be when the associated object is gone

-- Problem: How to handle player input?
-- What possible inputs are there?
-- * Player activates any ability or plays any card, only when they have priority
-- * Input required for activation of ability (targeting)
-- * Input required for resolution of ability
-- * Perhaps, we could create an IO zone. An ability that requires IO could place itself in that zone (only one at a time? or stack?). The IO zone can be checked before stepping through the simulation, and IO can be given. The IO zone contains an IO action that gets the required input and generates a Temporary to be added.

-- Problem: How to handle abilities that beget other abilities?
-- * Possibly related: Handling the stack. An ability (Transition) goes on the stack. There exists a Permanent rule that is applied whenever all players pass priority in succession, whose transition function takes the top ability off of the stack and then applies it (via composition).
-- * Abilities that beget other abilities can put them directly on the stack as part of their transition function, dur.

-- Flow
-- 1 Player activates ability (for our purposes this includes playing cards), or a triggered ability triggers
-- 1a If ability requires player input, it goes into the IO zone, and the game pauses while waiting for the input.
-- 2 Ability places the relevant transition function on the stack.
-- 3 Priority passes (uses IO zone)
-- 4 Topmost ability (transition) on the stack is applied.
-- 5 Priority goes to active player
-- 6 Go to 1
