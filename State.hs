{-# LANGUAGE ExistentialQuantification, TemplateHaskell, FlexibleInstances, MultiParamTypeClasses, FunctionalDependencies #-}
module State where

import Control.Lens
import Control.Monad.Trans.Class
import Control.Monad.Trans.State
--import Control.Monad.State.Class
import TStateT hiding (step)
import qualified TStateT as T (step)
import Data.Sequence (Seq)
import qualified Data.Sequence as S
import qualified Data.Foldable as F
import Data.Char (toLower)
import Data.Maybe

data MagicGame = MagicGame
    { _step :: (Step, Step, StepTime) -- prev step, next step
    , _players :: (Int, Seq Player) -- active player, list of players
    , _priority :: Maybe Int  -- index of player with priority
    , _winners :: Seq Player
    , _ioZone :: Maybe (IO (Temporary MagicGame))
    }
    deriving (Show)

instance Show (IO (Temporary MagicGame)) where
    show _ = "IO Action"

initialState :: MagicGame
initialState = MagicGame
    { _step = (Mulligan, Mulligan, STBeginning)
    , _players = (0, S.fromList [player1, player2])
    , _priority = Nothing
    , _winners = S.empty
    , _ioZone = Nothing
    }

data Player = Player
    { _life :: Integer
    , _passed :: Bool
    , _deck :: Deck
    , _hand :: Hand
    }
    deriving (Show, Eq)

-- Cards
data Card = Card
    { _cardTransitions :: Seq (Temporary MagicGame)
    , _cardName :: String
    , _cardPrevZone :: Zone
    }

instance Eq Card where
    (==) (Card{_cardName=a}) (Card{_cardName=b}) = a == b

instance Show Card where
    show (Card{_cardName=s}) = "Card {" ++ s ++ "}"

nullCard = Card 
    { _cardTransitions = S.singleton nullTransition 
    , _cardName = "Null"
    , _cardPrevZone = DeckZone
    }

data Zone = DeckZone
    deriving (Show, Eq)

data Deck = Deck
    { _deckCards :: Seq Card
    }
    deriving (Eq)

instance Show Deck where
    show (Deck{_deckCards = d}) = "Deck {" ++ show (S.length d) ++ " cards}"

data Hand = Hand 
    { _handCards :: Seq Card
    }
    deriving (Show, Eq)

player1 = Player
    { _life = 20
    , _passed = False
    , _deck = Deck $ S.fromList (replicate 60 nullCard)
    , _hand = Hand $ S.empty
    }

player2 = Player
    { _life = 20
    , _passed = False
    , _deck = Deck $ S.fromList (replicate 60 nullCard)
    , _hand = Hand $ S.empty
    }

data Phase = Beginning | Combat | Ending | MainPhase | Setup
    deriving (Show, Eq)
data Step = Untap | Upkeep | Draw | StartCombat | Attackers | Blockers | Damage | EndCombat | End | Cleanup | MainStep | Mulligan
    deriving (Show, Eq)
data StepTime = STBeginning | STMiddle | STEnd
    deriving (Show, Eq)

phase :: Step -> Phase
phase Untap = Beginning
phase Upkeep = Beginning
phase Draw = Beginning
phase StartCombat = Combat
phase Attackers = Combat
phase Blockers = Combat
phase Damage = Combat
phase EndCombat = Combat
phase End = Ending
phase Cleanup = Ending
phase MainStep = MainPhase
phase Mulligan = Setup -- phase where mulliganing happens

makeLenses ''MagicGame
makeLenses ''Player
makeLenses ''Hand
makeLenses ''Card
makeLenses ''Deck

--misc lenses
activePlayer :: Lens' MagicGame Player
activePlayer = lens get set
    where
        get (MagicGame {_players = (index, seq) }) = S.index seq index
        set g@(MagicGame {_players = (index, seq) }) player =
            g { _players = (index, S.update index player seq) }

--nextActivePlayer :: Lens' MagicGame Player
nextActivePlayer = get
    where
        get (MagicGame {_players = (index, seq) }) = S.index seq ((index+1) `mod` (S.length seq))

priorityPlayer :: Lens' MagicGame (Maybe Player)
priorityPlayer = lens get set
    where
        get :: MagicGame -> Maybe Player
        get (MagicGame {_priority = index, _players = (_,seq) }) = 
            case index of
                Just i -> Just $ S.index seq i
                Nothing -> Nothing
        set :: MagicGame -> Maybe Player -> MagicGame
        set g Nothing = g
        set g@(MagicGame {_priority = index, _players = (i',seq)}) (Just p) =
            case index of
                Nothing -> g
                Just i -> g {_players = (i', S.update i p seq)}

filterPlayers pred = players._2.to (S.filter pred)

--findPlayers pred = players._2.to (S

allPassed = players._2.to (F.foldl pred True)
    where
        pred b (Player {_passed = p}) = b && p

{--
somePassed = players._2.to (F.foldl pred False)
    where
        pred b (Player {_passed = p}) = b || p
--}

rules :: [Permanent MagicGame]
rules = 
    -- Turn structure
    [ Permanent (application [midOfStep, stepCon Mulligan, not . conAllPassed, isNothing . view ioZone], transition (promptDraw))
    , Permanent (application [begOfStep, stepCon Mulligan], transition (drawCards (players._2.traversed) 7))
    , Permanent (application [endOfStep, stepCon Mulligan], transition (stepTrans Untap))
    , Permanent (application [endOfStep, stepCon Untap], transition (stepTrans Upkeep))
    , Permanent (application [endOfStep, stepCon Upkeep, allPassedOrNothing], transition (stepTrans Draw))
    , Permanent (application [endOfStep, stepCon Draw, allPassedOrNothing], transition (stepTrans MainStep))
    -- Determine if we're on the first main step by checking if the previous step was part of the beginning, and we're currently on a main step
    , Permanent (application [endOfStep, stepCon MainStep, prevPhaseCon Beginning, allPassedOrNothing], transition (stepTrans StartCombat))
    , Permanent (application [endOfStep, stepCon StartCombat, allPassedOrNothing], transition (stepTrans Attackers))
    , Permanent (application [endOfStep, stepCon Attackers, allPassedOrNothing], transition (stepTrans Blockers))
    , Permanent (application [endOfStep, stepCon Blockers, allPassedOrNothing], transition (stepTrans Damage))
    , Permanent (application [endOfStep, stepCon Damage, allPassedOrNothing], transition (stepTrans EndCombat))
    , Permanent (application [endOfStep, stepCon EndCombat, allPassedOrNothing], transition (stepTrans MainStep))
    -- Determine if we're on any main step but the first  by checking if the previous step was not part of the beginning, and we're currently on a main step
    , Permanent (application [endOfStep, stepCon MainStep, not . prevPhaseCon Beginning, allPassedOrNothing], transition (stepTrans End))
    , Permanent (application [endOfStep, stepCon End, allPassedOrNothing], transition (stepTrans Cleanup))
    , Permanent (application [endOfStep, stepCon Cleanup], transition (stepTrans Untap >> playerNext))
    -- step timings
    -- - Beg -> Middle, empty stack
    -- - Middle -> End, empty stack, all passed
    -- - End -> Beg, empty Stack
    -- possibly abilities that trigger at beginning will 'resolve' during middle, trigger at end may resolve at next beg
    , Permanent (application [begOfStep], transition toSTMiddle)
    , Permanent (application [midOfStep, allPassedOrNothing], (transition (priority.=Nothing)) >+> transition toSTEnd)
    , Permanent (application [endOfStep], (transition (priority.=Nothing)) >+> transition toSTBeg)
    -- priority - players receive priority on every step except the untap and (usually) the cleanup
    -- players receive priority during the cleanup step ONLY if an activated or state-based action occurs during it. After everything is handled, another cleanup step begins
    -- reset priority only if we just entered the step and the stack is clean
    -- when we pass priority on the first player this gets applied after the priority modification is applied
    -- priority stages
    -- - beginning of all steps = Nothing
    -- - When not Untap and nothing is waiting to be put on the stack, and Nothing = first priority
    -- - After all players have passed - first priority
    -- beginning of step and stack is empty, priority = Nothing else stack is !empty, first
    -- end of step and stack is empty, priority = Nothing else stack is !empty, first
    , Permanent (application [not . stepCon Untap, condition (==) Nothing priority], transition firstPriority)
    , Permanent (application [not . (condition (==) True allPassed), condition (/=) Nothing priority, isNothing . view ioZone], transition (promptPass))
    , Permanent (application [condition (==) True allPassed, condition (/=) Nothing priority], transition firstPriority)
    -- Turn based actions
    , Permanent (application [begOfStep, stepCon Draw], transition (drawCards activePlayer 1))
    -- Win(loss) conditions
    , Permanent (application [condition (==) 1 ((filterPlayers (\(Player {_life=l}) -> l>0)).to S.length)], -- check if the number of players with positive life is 1
                 transition (putWinners))   -- put the winning players in the winner zone
    ]
    where
        -- when transitioning steps, reset the priority to Nothing
        toSTMiddle = step._3.=STMiddle
        toSTEnd = step._3.=STEnd
        toSTBeg = step._3.=STBeginning
        stepTrans s =
            do  cur <- use (step._2)
                step._1.= cur
                step._2.= s
                priority.= Nothing
        playerNext =
            do  cp <- use (players._1)
                ps <- use (players._2)
                players._1.= (cp + 1) `mod` S.length ps
        putWinners =
            do  ws <- use (players._2.to (S.filter (\(Player {_life=l}) -> l>0)))
                winners.= ws
        firstPriority =
            do  p <- use (players._1)
                priority .= Just p
                players._2.traversed.passed .= False
        promptPass = makeIO inp out
            where inp = putStrLn "Pass turn?" >> getLine
                  out s = if (map toLower s) == "pass" 
                            then
                                return $ Temporary (application [], removal [], transition (passPriority))
                            else
                                return $ nullTransition
        promptDraw = makeIO inp out
            where inp = putStrLn "Draw or Fin:" >> getLine
                  out s = case (map toLower s) of
                            "draw" -> return $ Temporary (application [], removal [], transition (drawHand))
                            "fin" -> return $ Temporary (application [], removal [], transition (passPriority))
                            otherwise -> return $ nullTransition
                  drawHand = 
                        do  curSize' <- use (priorityPlayer._Just.hand.handCards)
                            -- replace hand on deck
                            ch <- use (priorityPlayer._Just.hand.handCards)
                            priorityPlayer._Just.deck.deckCards %= (flip (S.><) $ ch)
                            let curSize = S.length curSize'
                            case curSize of
                                1 -> --if size is one, go to zero, switch player
                                    do  priorityPlayer._Just.hand.handCards.= S.empty
                                        passPriority
                                0 -> --if zero, at the beginning of the game, or priority is nothing (if it's nothing, this won't do anything)
                                    do  (h, d) <- use (priorityPlayer._Just.deck.deckCards.to (S.splitAt 7))
                                        priorityPlayer._Just.hand.handCards .= h
                                        priorityPlayer._Just.deck.deckCards .= d
                                otherwise ->
                                    do  (h, d) <- use (priorityPlayer._Just.deck.deckCards.to (S.splitAt (curSize - 1)))
                                        priorityPlayer._Just.hand.handCards .= h
                                        priorityPlayer._Just.deck.deckCards .= d

        passPriority =
            do  ps <- use (players._2)
                priorityPlayer %= fmap (\p -> p {_passed=True})
                priority %= fmap (\i -> (i+1) `mod` (S.length ps))
        drawCards playerLens n = playerLens %= doDraw
            where
                doDraw p@(Player{_hand=(Hand h), _deck=(Deck d)}) =
                    p{_hand=(Hand (h S.>< h')), _deck=(Deck d')}
                    where (h', d') = S.splitAt n d
                {--
        passActivePlayer =
            do  (a, ps) <- use (players)
                players._1 %= (\i -> (i+1) `mod` (S.length ps))
                --}
        stepCon s = condition (==) s (step._2)
        prevStepCon s = condition (==) s (step._1)
        phaseCon p = condition (==) p (step._1.to phase)
        prevPhaseCon p = condition (==) p (step._1.to phase)
        endOfStep = condition (==) STEnd (step._3)
        midOfStep = condition (==) STMiddle (step._3)
        begOfStep = condition (==) STBeginning (step._3)
        conAllPassed = condition (==) True allPassed
        allPassedOrNothing s = ((condition (==) Nothing priority) s) || (conAllPassed s)

condition comp value lens = comp value . view lens
application fs = Application (\s -> and (map ($ s) fs))
removal fs = Removal (\s -> and (map ($ s) fs))
--TODO take putIOZone out, make combinator for it. This was only for testing.
transition tf = Transition (execState (tf))
-- makeIO :: (IO a) -> (a -> IO (Temporary MagicGame)) -> IO (Temporary MagicGame)
makeIO input maker = ioZone .= Just (input >>= maker >>= makeRemoveAction)
    where
        makeRemoveAction (Temporary (app, rm, t)) = return $ Temporary (app, rm, transition removeIO >+> t)
removeIO = ioZone .= Nothing

-- ability targets are lenses?
-- things go on the stack, then when it's time to pop them off the stack, they go in the transition table as (const True, const True, f), so they are applied immediately and removed immediately.
-- Counterspells would go on the stack and during resolution would remove something from the stack, making sure it never ends up in the transition table
-- Modeling continous effects as transitions
-- * Figure out when the effect is applicable e.g. All white creatures get +1/+1. Applicable anytime the P/T is inspected. That is the application function.
-- * Related, temporary copies created whenever an object is inspected? That way transitions wouldn't have to include a "reversing" function.
-- * Related, look up automatically determining bijections.
-- * Zone for continuous effects? I think most can have removal be when the associated object is gone

-- Problem: How to handle player input?
-- What possible inputs are there?
-- * Player activates any ability or plays any card, only when they have priority
-- * Input required for activation of ability (targeting)
-- * Input required for resolution of ability
-- * Perhaps, we could create an IO zone. An ability that requires IO could place itself in that zone (only one at a time? or stack?). The IO zone can be checked before stepping through the simulation, and IO can be given. The IO zone contains an IO action that gets the required input and generates a Temporary to be added.

-- Problem: How to handle abilities that beget other abilities?
-- * Possibly related: Handling the stack. An ability (Transition) goes on the stack. There exists a Permanent rule that is applied whenever all players pass priority in succession, whose transition function takes the top ability off of the stack and then applies it (via composition).
-- * Abilities that beget other abilities can put them directly on the stack as part of their transition function, dur.

-- Flow
-- 1 Player activates ability (for our purposes this includes playing cards), or a triggered ability triggers
-- 1a If ability requires player input, it goes into the IO zone, and the game pauses while waiting for the input.
-- 2 Ability places the relevant transition function on the stack.
-- 3 Priority passes (uses IO zone)
-- 4 Topmost ability (transition) on the stack is applied.
-- 5 Priority goes to active player
-- 6 Go to 1

-- Drawing cards
-- Problems:
-- - How to tell if a card was drawn? Added prevZone state to card. Is updated to match the zone it is in if it doesn't match already. Check this to see if card has been drawn this turn.
-- - How to tell difference between drawn and "put into hand"? Set prevZone in the "put into hand" operation. Alternatively, have a LookingAt zone as cards that are put into hand are usually looked at first.
-- - How to model cards like Omen Machine, "Player can't draw cards"? Simple way is to check for any drawn cards and put them back in the library. Not very good though. Possibly figure out some way of testing transitions before they are applied, and if the new state is illegal for some reason, don't apply the transition, otherwise apply it.
-- -- Specify properties that have to hold? E.g. 
-- -- prop_NoDraw :: MagicGame -> Bool
-- -- prop_NoDraw = anyOf (players._2.traversed.hand.handCards.traversed.cardPrevZone) (DeckZone==)

properties =
    [ Property (Removal (const False), Prop prop_NoDraw)
    ]
    where
        prop_NoDraw = allOf (players._2.traversed.hand.handCards.traversed.cardPrevZone) (DeckZone/=)
