module Paths_magic_simulator (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch


version :: Version
version = Version {versionBranch = [0,1,0,0], versionTags = []}
bindir, libdir, datadir, libexecdir :: FilePath

bindir     = "/home/dwilson/.cabal/bin"
libdir     = "/home/dwilson/.cabal/lib/magic-simulator-0.1.0.0/ghc-7.6.3"
datadir    = "/home/dwilson/.cabal/share/magic-simulator-0.1.0.0"
libexecdir = "/home/dwilson/.cabal/libexec"

getBinDir, getLibDir, getDataDir, getLibexecDir :: IO FilePath
getBinDir = catchIO (getEnv "magic_simulator_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "magic_simulator_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "magic_simulator_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "magic_simulator_libexecdir") (\_ -> return libexecdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
