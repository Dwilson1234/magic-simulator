{-# LANGUAGE FlexibleInstances, GeneralizedNewtypeDeriving, MultiParamTypeClasses #-}

{--
 - A monad transformer for state machines with a modifiable transition table and modifiable property table.
 -
 - The TStateT, MonadState instance, runTStateT, and step are all essentially from SO
 - here: http://stackoverflow.com/questions/19484770/state-monad-with-predicates.
 - I'm still working on understanding those.
--}
module TStateT (
    Removal(..),
    Application(..),
    Transition(..),
    Permanent(..),
    Temporary(..),
    Prop(..),
    Property(..),
    TStateT(..),
    runTStateT,
    step,
    addTransition,
    addProperty,
    nullTransition,
    (>+>)
) where

import Control.Arrow
import Control.Applicative
import Control.Monad.State
import Data.Maybe
import Data.List (foldl')

-- TODO: Restructure this :P there has to be a pattern in here somewhere
-- TODO: These should probably all implement Functor and Applicative
newtype Removal s = Removal (s -> Bool)
newtype Application s = Application (s -> Bool)
newtype Transition s = Transition (s -> s)
newtype Prop s = Prop (s -> Bool)   -- given a state, is this property violated?

(>+>) :: Transition s -> Transition s -> Transition s
(>+>) (Transition f) (Transition b) = Transition (f . b)

newtype Permanent s = Permanent (Application s, Transition s) -- permanent transitions don't have a removal criteria
newtype Temporary s = Temporary (Application s, Removal s, Transition s)
newtype Property s = Property (Removal s, Prop s)

newtype TStateT s m a = TStateT { unTStateT :: StateT (s, ([Permanent s], [Temporary s], [Property s])) m a }
    deriving (Functor, Applicative, Monad, MonadTrans, MonadIO)

instance Monad m => MonadState s (TStateT s m) where
    state f = TStateT (state (\(s, t) -> let (v, s') = f s in (v, (s', t))))

-- take an initial list of permanent transition functions
runTStateT :: Functor m => ([Permanent s], [Property s]) -> TStateT s m a -> s -> m (a, s)
runTStateT (perms, props) m s = (fst <$>) <$> runStateT (unTStateT m) (s, (perms, [], props))

-- I want to strip transitions and modify the state at the same time, so I thread the existing state through the computation
step :: Monad m => TStateT s m ()
step = TStateT (gets getTs) >>= \(s, x) -> modify x >>= \_ -> TStateT (modify (ungetTs s))

-- TODO: instead of composing all the transition functions, apply the transition to the state manually and return the new state
--       if properties fail, use last good state.
-- how do we go about defining the order of application of transitions?
getTs :: (s, ([Permanent s], [Temporary s], [Property s])) -> (s, s -> s)
getTs (s, (ps, ts, props)) = (s, (foldl' f id ps) . (foldl' g id ts))
    where
        f t (Permanent (Application p, Transition t'))    = if p s && doProps t' then t.t' else t
        g t (Temporary (Application p, _, Transition t')) = if p s && doProps t' then t.t' else t
        doProps t = not $ any not $ map (\ (Property (_, Prop p)) -> p $ t s) props

-- threaded state is used instead of pre-existing one, make sure to return existing state
ungetTs :: s -> (s, ([Permanent s], [Temporary s], [Property s])) -> (s, ([Permanent s], [Temporary s], [Property s]))
ungetTs s (s', (ps, ts, props)) = (s', (ps, filter f ts, filter p props))
    where
        -- only strip things from the temporaries
        f (Temporary (_, Removal p, _)) = not $ p s
        p (Property (Removal p, _)) = not $ p s

-- Possibly add an id for each transition
addTransition :: Monad m => Temporary s -> TStateT s m ()
addTransition = TStateT . modify . (\t (s, (ps, ts, props)) -> (s, (ps, t:ts, props)))

addProperty :: Monad m => Property s -> TStateT s m ()
addProperty = TStateT . modify . (\p (s, (ps, ts, props)) -> (s, (ps, ts, p:props)))

nullTransition = Temporary (Application (const False), Removal (const True), Transition id)
