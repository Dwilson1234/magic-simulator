{-# LANGUAGE ExistentialQuantification #-}

{--
 - A monad transformer for state machines with a modifiable transition table and modifiable property table.
 -
 - The TStateT, MonadState instance, runTStateT, and step are all essentially from SO
 - here: http://stackoverflow.com/questions/19484770/state-monad-with-predicates.
 - I'm still working on understanding those.
--}
module Scratch (
    SWhole (SNone),
    Tag (..),
    StatePiece (..),
    addState,
    inspect,
    require,
    provide
) where

import Control.Arrow
import Control.Applicative
import Control.Monad.State
import Data.Maybe
import Control.Lens.Getter
import Data.List (foldl')

-- thisModule . requires [Poison, Core] . andProvides state

-- newState :: IO (IORef (SWhole))
-- newState = newIORef SNone

-- requires :: [Tag]

-- provide :: StatePiece

data SWhole = SNone | SCons (StatePiece) (SWhole)
data Tag = Tag String
    deriving (Show, Eq, Ord)

instance Show (SWhole) where
    show (SNone) = "SNone"
    show (SCons x s) = "SCons { " ++ show x ++ ", " ++ show s ++ " }"

data StatePiece = forall a . (Show a) => StatePiece
    { spProps :: [a -> Bool]
    , sp :: a
    , tag :: Tag
    }

instance Show (StatePiece) where
    show (StatePiece a x t) = "StatePiece { " ++ show (length a) ++ " properties, " ++ show x ++ ", " ++ show t ++ " }"

addState :: StatePiece -> SWhole -> SWhole
addState sp@(StatePiece {tag = t}) sw = 
    case (inspect sw t) of
        Nothing -> SCons sp sw
        _ -> sw

inspect :: SWhole -> Tag -> Maybe (StatePiece)
inspect SNone _ = Nothing
inspect (SCons sp@(StatePiece {tag = t}) s) t'
    | t == t' = Just sp
    | otherwise = inspect s t'

valid :: SWhole -> Bool
valid SNone = True
valid (SCons sp next) = validPiece sp && valid next

validPiece :: StatePiece -> Bool
validPiece (StatePiece ps s t) = and $ map ($ s) ps

{--
permanent = Tagged []
temporary = Tagged
property r a = Tagged r a id

newtype TStateT s m a = TStateT { unTStateT :: StateT (s, SM s) m a }
    deriving (Functor, Applicative, Monad, MonadTrans, MonadIO)

instance Monad m => MonadState s (TStateT s m) where
    state f = TStateT (state (\(s, t) -> let (v, s') = f s in (v, (s', t))))
-- take an initial list of permanent transition functions
runTStateT :: Functor m => [Permanent s] -> [Property s] -> TStateT s m a -> s -> m (a, s)
runTStateT (perms, props) m s = (fst <$>) <$> runStateT (unTStateT m) (s, (perms, [], props))

-- I want to strip transitions and modify the state at the same time, so I thread the existing state through the computation
step :: Monad m => TStateT s m ()
step = TStateT (gets getTs) >>= \(s, x) -> modify x >>= \_ -> TStateT (modify (ungetTs s))

-- TODO: instead of composing all the transition functions, apply the transition to the state manually and return the new state
--       if properties fail, use last good state.
-- how do we go about defining the order of application of transitions?
getTs :: (s, ([Permanent s], [Temporary s], [Property s])) -> (s, s -> s)
getTs (s, (ps, ts, props)) = (s, (foldl' f id ps) . (foldl' g id ts))
    where
        f t (Permanent (Application p, Transition t'))    = if p s && doProps t' then t.t' else t
        g t (Temporary (Application p, _, Transition t')) = if p s && doProps t' then t.t' else t
        doProps t = not $ any not $ map (\ (Property (_, Prop p)) -> p $ t s) props

-- threaded state is used instead of pre-existing one, make sure to return existing state
ungetTs :: s -> (s, ([Permanent s], [Temporary s], [Property s])) -> (s, ([Permanent s], [Temporary s], [Property s]))
ungetTs s (s', (ps, ts, props)) = (s', (ps, filter f ts, filter p props))
    where
        -- only strip things from the temporaries
        f (Temporary (_, Removal p, _)) = not $ p s
        p (Property (Removal p, _)) = not $ p s

-- Possibly add an id for each transition
addTransition :: Monad m => Temporary s -> TStateT s m ()
addTransition = TStateT . modify . (\t (s, (ps, ts, props)) -> (s, (ps, t:ts, props)))

addProperty :: Monad m => Property s -> TStateT s m ()
addProperty = TStateT . modify . (\p (s, (ps, ts, props)) -> (s, (ps, ts, p:props)))

nullTransition = Temporary (Application (const False), Removal (const True), Transition id)
--}
